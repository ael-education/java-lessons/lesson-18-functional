/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package ru.vertexprise.javafunctionalexamples;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author developer
 */
public class JavaFunctionalExamples {

    public static void main(String[] args) {
         
        // Список преподавателей 
        List<Professor> professorList = new ArrayList<>();
        
        Professor prof1 = new Professor();
        prof1.setFirstName("Иван");
        prof1.setSureName("Петров");
        prof1.setMiddleName("Петрович");
        prof1.setDepartment("Кафедра цифрового, государственного и корпоративного управления");
        prof1.setPosition("Профессор");
        
        
        Professor prof2 = new Professor();
        prof2.setFirstName("Дмитрий");
        prof2.setSureName("Ваганов");
        prof2.setMiddleName("Валерьевич");
        prof2.setDepartment("Кафедра цифрового, государственного и корпоративного управления");
        prof2.setPosition("Старший преподаватель");
        
        professorList.add(prof1);
        professorList.add(prof2);
        
        
        // Сформирован пустой список предикатов
        List<Predicate<Professor>> allPredicates = new ArrayList<Predicate<Professor>>();
        
        String firstName = "Иван";
        String sureName = "Петров";
        
        
        allPredicates.add(p -> p.getFirstName().equalsIgnoreCase(firstName));
        allPredicates.add(p -> p.getSureName().equalsIgnoreCase(sureName));
       
        List<Professor> result = professorList.stream()
                .filter(allPredicates
                        .parallelStream()
                        .reduce(x->true, Predicate::and))               
                .collect(Collectors.toList());
        
        System.out.println("Наден список ["+result.size()+"] записей");
         if (!result.isEmpty())
         {
             System.out.println(result.get(0));
         }  
        
  
        
        
    }
}
